# MeetFrank Full-stack Test

### Overview

The task is to create a simple realtime chat app (Figma link for views is provided below)

App consists of 3 screens:

* Intro screen, where user has to input his/her name. User session should be created.
* Online users list, where you can see all of the people who have this app open in their browser. If there are unread messages then a "badge" is shown. You can click on each user and enter a Chat screen.
* Chat screen, where you see all historical chat messages with this user. On the bottom of the screen you can send a new message using a text input and a "Send" button.

### Tech
Tech to use for this task:

* For front-end please use React
* For back-end please use Node.js
* Everything else is up to you

### Links
* [Views](https://www.figma.com/file/Z7v6ZR4xk7kqhH4Wf1UngJ/chat?node-id=0%3A1) - Figma link for views (If you login to figma with your gmail account you can then select different components and see what styles have been applied, how they are positioned etc...)

### Sending completed task
You can send the completed task to anton@meetfrank.com and add kaspar@meetfrank.com, fabian@meetfrank.com to CC.
It can be in a form of a git repo link, zipped folder or a fileshare link.